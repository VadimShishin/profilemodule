SELECT *
FROM flowers;

SELECT *
FROM clients;

SELECT *
FROM orders;

-- 1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
SELECT o.id, o.date, f.flower, o.amount, o.total, c.name, c.phone
FROM orders o
    inner join clients c on c.id = o.client_id
    inner join flowers f on f.id = o.flower_id
    WHERE o.id = 4;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
SELECT o.id, o.date,  c.name, f.flower, o.amount, o.total, c.phone
FROM orders o
    INNER JOIN clients c on c.id = o.client_id
    INNER JOIN flowers f on f.id = o.flower_id
    WHERE c.id = 1
    AND date >= now() - INTERVAL '1 MONTH';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
SELECT o.id, o.date, f.flower, o.amount, o.total, c.name, c.phone
FROM orders o
    INNER JOIN clients c on c.id = o.client_id
    INNER JOIN flowers f on f.id = o.flower_id
    WHERE amount = (SELECT max(amount) FROM orders LIMIT 1);

--4. Вывести общую выручку за все время
SELECT sum(total) as "Общая выручка"
FROM orders o
    INNER JOIN flowers f on f.id = o.flower_id