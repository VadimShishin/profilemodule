create table public.flowers
(
    id          SERIAL PRIMARY KEY,
    flower      VARCHAR(100) UNIQUE check (flower <> '') NOT NULL,
    price       INTEGER NOT NULL
);

create table public.clients
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(50) NOT NULL,
    phone       VARCHAR(20) CHECK (phone ~ '\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}') NOT NULL
);

create table public.orders
(
    id          SERIAL PRIMARY KEY,
    date        TIMESTAMP NOT NULL,
    client_id   INTEGER REFERENCES clients(id) NOT NULL,
    flower_id   INTEGER REFERENCES flowers(id) NOT NULL,
    amount      INTEGER CHECK (amount >= 1 AND amount <= 1000) NOT NULL,
    total       INTEGER NOT NULL
);

drop table orders;
drop table clients;
drop table flowers;