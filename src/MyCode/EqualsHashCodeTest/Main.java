package MyCode.EqualsHashCodeTest;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Vadim666", "Vadim", "Omsk");
        User user2 = new User("Vadim666", "Vadim", "Omsk");

        boolean equalsValue = user1.equals(user2);

        System.out.println("equalsValue: " + equalsValue);
        System.out.println("user1.hashCode(): " + user1.hashCode());
        System.out.println("user2.hashCode(): " + user2.hashCode());
    }
}
