package MyCode.EqualsHashCodeTest;

import java.util.Objects;

public class User {

    private String login;
    private String name;
    private String city;

    public User(String login, String name, String city) {
        this.login = login;
        this.name = name;
        this.city = city;
    }

    @Override
    public boolean equals(Object obj) { //переопределение метода equals
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }

        User other = (User) obj; // сравнение по полям, если выше приведённые проверки прошли успешно
        return login.equals(other.login) && name.equals(other.name) && city.equals(other.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, name, city);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
