package MyCode.StreamTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest1 {
    public static void main(String[] args) {

        // Вывод значений
        Stream<Integer> stream1 = Stream.of(1, 2, 3, 4, 5);
        stream1.forEach((Integer digit) -> {
            System.out.print(digit);
        });
        System.out.println();

        // То же самое, что и выше, но упрощённый вариант написания
        Stream<Integer> stream2 = Stream.of(1, 2, 3, 4, 5);
        stream2.forEach(System.out::print);
        System.out.println();

        // Фильтр вывести числа больше 2 и меньше 5
        Stream<Integer> stream3 = Stream.of(1, 2, 3, 4, 5);
        stream3.filter((Integer digit) -> { // Длинный вариант
                    if (digit > 2) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .filter((Integer digit) -> { // Упрощённый вариант
                    return digit < 5;
                })
                .forEach(System.out::print);
        System.out.println();

        // Преобразование к другому типу. В данном случае из инт к стринг
        Stream<Integer> stream4 = Stream.of(1, 2, 3, 4, 5);
        Stream<String> newStream = stream4.map((Integer digit) -> {
            return "str: " + digit + ". ";
        });

        newStream.forEach(System.out::print);
        System.out.println();

        // Всё что выше, но упрощённая запись
        Stream.of(1, 2, 3, 4, 5)
                .filter((Integer digit) -> { // Все значения, что больше 2
                    return digit > 2;
                })
                .map((Integer digit) -> { // Преобразование к String
                    return "newStr: " + digit + ". ";
                })
                .forEach(System.out::print); // Вывод на экран
        System.out.println();

        // НЕ вывод в консоль, как в прошлом примере, а добавление в List
        List<String> list1 = Stream.of(1, 2, 3, 4, 5)
                .filter((Integer digit) -> { // Все значения, что больше 2
                    return digit > 2;
                })
                .map((Integer digit) -> { // Преобразование к String
                    return "newStr: " + digit + ". ";
                })
                .collect(Collectors.toList()); // Добавление в List

        // Упрощённая запись примера выше
        List<String> list2 = Stream.of(1, 2, 3, 4, 5)
                .filter(digit -> digit > 2)
                .map(digit -> "newStr: " + digit + ". ")
                .toList();

        list2.forEach(System.out::print);
        System.out.println();

        // Другой пример работы с листом
        // ГЛАВНОЕ!!! Стрим не изменяет коллекцию, в которой он вызывался
        List<Integer> list3 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list3.add(i);
        }

        list3.stream()
                .filter(digit -> digit >= 2 && digit <= 4)
                .map(digit -> digit * 2)
                .forEach(System.out::print); // Выводит все действия проведённые выше, но не изменяет list
        System.out.println();
        System.out.println(list3);
        System.out.println();

        // Другой пример работы с листом. ВАРИАНТ 2
        List<Integer> list4 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list4.add(i);
        }

        List<Integer> anotherList = list4.stream() // создаётся новый лист, в котором буду изменены значения
                .filter(digit -> digit >= 2 && digit <= 4)
                .map(digit -> digit * 2)
                .toList();
        System.out.println(list4);
        System.out.println(anotherList);
        System.out.println();
    }
}