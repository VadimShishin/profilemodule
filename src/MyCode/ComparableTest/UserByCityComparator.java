package MyCode.ComparableTest;


import java.util.Comparator;

public class UserByCityComparator implements Comparator<User> {

    @Override
    public int compare(User lhs, User rhs) {
        return lhs.getCity().compareToIgnoreCase(rhs.getCity());
    }
}
