package MyCode.ComparableTest;


import java.util.Comparator;

public class UserByAgeComparator implements Comparator<User> {

    @Override
    public int compare(User lhs, User rhs) {
        if (lhs.getAge() == rhs.getAge()) {
            return 0;
        }

        if (lhs.getAge() < rhs.getAge()) {
            return -1;
        } else {
            return 1;
        }
    }
}
