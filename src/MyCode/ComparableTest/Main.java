package MyCode.ComparableTest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> list = new ArrayList<>();
        list.add(new User("Vadim", 35, "Omsk"));
        list.add(new User("Ilya", 33, "Pavlodar"));
        list.add(new User("Alexey", 34, "Moskow"));

//        list.sort(User::compareTo); // сортировка описанная в классе user

        System.out.println("list: " + list);

        Comparator<User> cmpByAge = new UserByAgeComparator();
        Comparator<User> cmpByCity = new UserByCityComparator();

//        list.sort(cmpByAge);
        list.sort(cmpByCity);

        System.out.println("After: " + list);
    }
}