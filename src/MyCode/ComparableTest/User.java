package MyCode.ComparableTest;
import java.util.Comparator;

public class User implements Comparable<User> {
    private String name;
    private int age;
    private String city;

    public User(String name, int age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

/*    @Override
        public int compareTo(User other) { // сортировка по полю age
        if (this.age == other.age){
            return 0;
        }

        if (this.age < other.age) {
            return -1;
        } else {
            return 1;
        }
    }*/

    @Override
    public int compareTo(User other) { // сортировка по полю city без учёта регистра
        return this.city.compareToIgnoreCase(other.city);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }
}