package MyCode.SetTreeSetSortedSetNavigableSet;

import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        System.out.println("Set - TreeSet"); // заголовок

        Set<Integer> set = new TreeSet<>();
        for (int i = 10; i >= 1; --i) {  //добавление элементов
            set.add(i);
        }

        System.out.print("add numbers in set: ");
        for (Integer digit : set) { //вывод элементов
            System.out.print(digit + " ");
        }

        System.out.println();
        System.out.println("---------------------------------------------------------");
//---------------------------------------------------------
        System.out.println("SortedSet - TreeSet"); // заголовок

        SortedSet<Integer> set2 = new TreeSet<>();

        for (int i = 10; i >= 1; --i) { //добавление элементов
            set2.add(i);
        }

        System.out.print("add numbers in set2: ");
        for (Integer digit : set2) {  //вывод элементов
            System.out.print(digit + " ");
        }
        System.out.println();


        Integer firstValue = set2.first(); // присваивает первый элемент
        Integer lastValue = set2.last(); // присваивает последний элемент

        System.out.println("firstValue: " + firstValue);
        System.out.println("lastValue: " + lastValue);

        System.out.println("---------------------------------------------------------");

        SortedSet<Integer> subSet = set2.subSet(2, 9);
        System.out.println("subSet: " + subSet);

        System.out.println("---------------------------------------------------------");

        set2.remove(5);

        System.out.println("удаление 5 из set2: " + set2);

        SortedSet<Integer> subSet2 = set2.subSet(4, 6); // так как 5 удалена, то она и не будет выводиться
        System.out.println("subSet2 от 4 до 6: " + subSet2);

        System.out.println("---------------------------------------------------------");

        SortedSet<Integer> headSet = set2.headSet(5);
        System.out.println("headSet до 5: " + headSet);
        SortedSet<Integer> tailSet = set2.tailSet(7);
        System.out.println("tailSet от 7: " + tailSet);

        System.out.println("---------------------------------------------------------");
//---------------------------------------------------------
        System.out.println("NavigableSet - TreeSet"); // заголовок

        NavigableSet<Integer> set3 = new TreeSet<>(); //добавление элементов
        for (int i = 10; i >= 1; --i) {
            set3.add(i);
        }

        System.out.print("add numbers in set3: ");
        for (Integer digit : set3) {  //вывод элементов
            System.out.print(digit + " ");
        }
        System.out.println();

        Integer higherValue = set3.lower(5);
        System.out.println("higherValue: " + higherValue); //если > 5

        Integer lowerValue = set3.higher(5);
        System.out.println("lowerValue: " + lowerValue); // если < 5

        Integer ceilingValue = set3.ceiling(5);
        System.out.println("ceilingValue: " + ceilingValue); // min (elem >= 5)

        Integer floorValue = set3.floor(5);
        System.out.println("floor: " + floorValue); // max (elem <= 5)

        NavigableSet<Integer> subSet3 = set3.subSet(2, true, 8, false);
        System.out.println("subSet от 2 до 8: "+ subSet3);

        NavigableSet<Integer> subSet4 = set3.subSet(2, true, 8, true);
        System.out.println("subSet от 2 до 8 включительно: "+ subSet4);

        NavigableSet<Integer> tailSet2 = set3.tailSet(8, true);
        System.out.println("tailSet от 8 включительно: "+ tailSet2); // также и headSet. true включает, false не включает

        SortedSet<Integer> reversSet = set3.descendingSet();
        System.out.println("reversSet: " + reversSet);

    }
}
