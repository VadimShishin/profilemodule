package MyCode.ComparableList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ComparatorInList {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(0);
        list.add(5);
        list.add(4);

        System.out.println("list: " + list);

        Comparator<Integer> cmp = (Integer lhs, Integer rhs) -> {
            if (lhs.equals(rhs)) {
                return 0;
            }
            if (lhs < rhs) {
                return -1; //можно исправить на 1, а в else исправить на -1 и тогда будет считать в обратном порядке
            } else {
                return 1;
            }
        };

        list.sort(cmp);

        System.out.println("list: " + list);

        System.out.println();

        // сортировка с помощью компаратора
        System.out.println("Comparator");

        List<String> list2 = new ArrayList<>();
        list2.add("2");
        list2.add("1");
        list2.add("3");

        Comparator<String> test11 = (String lhs, String rhs) -> {
            return lhs.compareTo(rhs);
//            return -1 * lhs.compareTo(rhs); // в обратном порядке
        };

        list2.sort(test11);

        list2.forEach((String s) -> {
            System.out.print(s);
            System.out.println(" ");
        });


        //----------------------------------------------------------------

        List<String> list3 = new ArrayList<>();
        list3.add("AAA");
        list3.add("AA");
        list3.add("A");

        Comparator<String> test12 = (String lhs, String rhs) -> {
            int lhsSize = lhs.length();
            int rhsSize = rhs.length();

            if (lhsSize == rhsSize) {
                return 0;
            }

            if (lhsSize < rhsSize) {
                return -1;
            } else {
                return 1;
            }
        };

        list3.sort(test12);

        list3.forEach((String s) -> {
            System.out.print(s);
            System.out.println(" ");
        });
    }
}
