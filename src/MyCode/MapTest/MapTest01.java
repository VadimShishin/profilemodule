package MyCode.MapTest;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MapTest01 {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        System.out.println("size: " + map.size());
        System.out.println("isEmpty: " + map.isEmpty());

        map.put(1, "1"); //Добавить значение
        System.out.println("map: " + map);
        System.out.println(map.size()); // Вывести размер map
        System.out.println(map.isEmpty()); // Вывести boolean значение. Пуста ли map?

        for (int i = -2; i <= 5; i++) { //Добавить значения
            map.put(i, "Value:" + i);
        }

        System.out.println();
        System.out.println("--------Содержит ли map определённый ключ?");
        System.out.println("Текущие ключи и значения map: " + map);
        System.out.println("map.containsKey(2): " + map.containsKey(2));
        System.out.println("map.containsKey(-5): " + map.containsKey(-5));

        System.out.println();
        System.out.println("--------Содержит ли map определённое значение?");
        System.out.println("map.containsValue(\"Value:2\"): " + map.containsValue("Value:2"));
        System.out.println("map.containsValue(\"Value:10\"): " + map.containsValue("Value:10"));

        String result = map.put(4, "NEW Value:4"); // присвоит предыдущее значение в переменную и добавит новое в указанный ключ
        System.out.println("result: " + result);
        System.out.println(map.get(4));

        System.out.println();
        System.out.println("Присвоит значение boolean. true если есть значение, false если нет: ");
        boolean result2 = map.remove(4, "abc"); // Присвоит значение boolean. true если есть значение, false если нет
        System.out.println("map.remove(4, \"abc\"): " + result2);

        System.out.println();
        System.out.println("Присвоит предыдущее значение в переменную и добавит новое в указанный ключ, если ключ существует. Иначе не отработает и вернёт null: ");
        String result3 = map.replace(5, "NEW Value:5"); // Присвоит предыдущее значение в переменную и добавит новое в указанный ключ, если ключ существует. Иначе не отработает и вернёт null
        System.out.println("map.replace(5, \"NEW Value:5\"): " + result3);

        System.out.println();
        System.out.println("Если в указанном ключе есть значение oldValue (abc), то присвоит ключу newValue. Если значение oldValue отсутствует, то присвоит переменно false и не изменит коллекцию, если есть, то переменной присвоит true и изменит значение на newValue: ");
        boolean result4 = map.replace(1, "abc", "NEW Value:1"); // Если в указанном ключе есть значение oldValue (abc), то присвоит ключу newValue. Если значение oldValue отсутствует, то присвоит переменно false и не изменит коллекцию, если есть, то переменной присвоит true и изменит значение на newValue
        System.out.println("map.replace(1, \"abc\", \"NEW Value:1\"): " + result4);

        System.out.println();
        System.out.println("-----------Вывод ключей и значений");

        Set<Integer> keys = map.keySet();
        Collection<String> values = map.values();

        System.out.println("1 вариант: "); // 1 вариант вывода

        for (Integer key : keys) {        // Вывод ключей и значений
            System.out.println("key: " + key + " value: " + map.get(key));
        }

        System.out.println();
        System.out.println("только ключи: ");
        ;
        for (String value : values) {        // Вывод значений
            System.out.println("value: " + value);
        }

        System.out.println();
        System.out.println("2 вариант: ");
        System.out.println("--------С помощью метода entry можно вывести key, value и изменить value. Изменить key невозможно.");
        // С помощью метода entry можно вывести key, value и изменить value. Изменить key невозможно
        Set<Map.Entry<Integer, String>> entries = map.entrySet();
        for (Map.Entry<Integer, String> entry : entries) { // Вывод ключей и значений (Лучше, чем предыдущий вариант)
            Integer key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + " : " + value);
        }

        Map<Integer, String> map2 = new HashMap<>(); // Новая map
        for (int i = -2; i <= 5; i++) {
            map2.put(i, "Value:" + i);
        }

        Set<Map.Entry<Integer, String>> entries2 = map2.entrySet();
        for (Map.Entry<Integer, String> entry : entries2) { // Изменение value
            Integer key = entry.getKey();
            String value = entry.getValue();
            entry.setValue("NEW " + value);
        }
        System.out.println(map2);

        System.out.println();
        System.out.println("-------Новая map (LinkedHashMap). Значения выведены в том порядке, в котором добавлялись:");
        Map<Integer, String> map3 = new LinkedHashMap<>(); // Новая map (LinkedHashMap)
        for (int i = -2; i <= 5; i++) {
            map3.put(i, "Value:" + i);
        }
        System.out.println(map3); //Значения выведены в том порядке, в котором добавлялись

        System.out.println();
        System.out.println("-------Новая map (TreeMap). Значения выведены в порядке возрастания ключей:");
        Map<Integer, String> map4 = new TreeMap<>(); // Новая map (TreeMap)
        for (int i = 5; i >= -2; i--) {
            map4.put(i, "Value:" + i);
        }
        System.out.println(map4); //Значения выведены в порядке возрастания ключей (в случае если ключ Integer)

        System.out.println();
        Map<Integer, String> map5 = new TreeMap<>(); // Новая map (HashMap)
        for (int i = 0; i <= 3; i++) {
            map5.put(i, "Value:" + i);
        }
        System.out.println("Значения map5: " + map5);

        Map<Integer, String> map6 = new HashMap<>(); // Новая map (HashMap). Неважно какая будет добавляемая map (TreeMap, LinkedHashMap, HashMap)
        for (int i = 4; i <= 6; i++) {
            map6.put(i, "Value:" + i);
        }
        System.out.println("Значения map6: " + map6);

        map5.putAll(map6);
        System.out.println("После добавления map6 в map5: " + map5);

        System.out.println();
        System.out.println("------------Метод getOrDefault. Если указанного key нет, то возвращает value, указанное в методе (метод get вернул бы null). Если key есть, то возвращает value данного key");

        // 1 пример
        Map<String, Integer> map7 = new HashMap<>(); // Новая map (HashMap).
        Integer value = map7.getOrDefault("key1", -1); // Если ключа "key1" нет, то вернёт -1. Метод get вернул бы null
        System.out.println("value: " + value);

        // 2 пример
        String[] array = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
        Map<String, Integer> map71 = new HashMap<>();
        for (String tmp : array) {
            map71.put(tmp, map71.getOrDefault(tmp, 0) + 1); // если ключа tmp нет, то вернёт 0 + 1, если ключ есть, то вернёт его значение + 1.
        }

        System.out.println();
        System.out.println("--------Метод merge. Добавляет value. Если value у данного key есть, то даёт возможность принять выбор какое значение нужно установить или оставить имеющееся");

        Map<String, Integer> map8 = new HashMap<>(); // Новая map (HashMap).
        map8.put("key1", 123);
        map8.put("key2", 234);

        BiFunction<Integer, Integer, Integer> mergeFunction = (Integer oldValue, Integer newValue) -> { // устанавливает требуемое value в случае коллизии
            System.out.println("Inside merge function: oldValue = " + oldValue + ", newValue = " + newValue);
            return oldValue + newValue;
        };

        //map8.merge("key5", 100, mergeFunction);
        Integer result5 = map8.merge("key1", 100, mergeFunction);
        System.out.println("result5: " + result5);
        System.out.println("map" + map8);

        System.out.println();
        System.out.println("--------Метод compute. Изменяет value по-указанному key, также производит различные действия как с value, так и доп действия");

        Map<String, Integer> map9 = new HashMap<>(); // Новая map (HashMap).
        map9.put("key1", 123);
        map9.put("key2", 234);

        BiFunction<String, Integer, Integer> computeFunction = (String key, Integer oldValue) -> { // Изменяет value по-указанному key, также производит различные действия
            System.out.println("Inside compute function: key = " + key + ", oldValue = " + oldValue);
            return oldValue * 2;
        };

        Integer result6 = map9.compute("key1", computeFunction);
        // Compute работает всегда.
        // computeIfAbsent работает также, но только если данного ключа нет в map

        System.out.println("result5: " + result6);
        System.out.println("map" + map9);

        System.out.println();
        System.out.println("--------Метод computeIfPresent. Сработает также как и compute, но только если указанный ключ есть в map. Пишется также как и compute");

        System.out.println();
        System.out.println("--------Метод computeIfAbsent. Сработает также как и compute, но только если указанного ключа нет в map");

        Map<String, Integer> map10 = new HashMap<>(); // Новая map (HashMap).
        map10.put("key1", 123);

        Function<String, Integer> computeIfAbsentFunction = (String key) -> {
            System.out.println("Inside computeIfAbsentFunction function: key = " + key);
            return 150;
        };

        Integer result7 = map10.computeIfAbsent("key1", computeIfAbsentFunction);
        Integer result8 = map10.computeIfAbsent("key2", computeIfAbsentFunction);

        System.out.println("map" + map10);

        System.out.println();
        System.out.println("------------------------------------------------");
        System.out.println("Тема SortedMap");

        SortedMap<Integer, String> sortedMap = new TreeMap<>(); // SortedMap

        for (int i = 0; i <= 5; i++) {
            sortedMap.put(i, "Value: " + i);
        }

        System.out.println("sortedMap: " + sortedMap);
        System.out.println("метод sortedMap.firstKey(): " + sortedMap.firstKey());
        System.out.println("метод sortedMap.lastKey(): " + sortedMap.lastKey());

        SortedMap<Integer, String> submap = sortedMap.subMap(1, 3);  // берёт значения из другой мап в указанном интервале
        System.out.println("submap от 1 до 3: " + submap);

        SortedMap<Integer, String> headMap = sortedMap.headMap(3);  // берёт все значения из другой мап до указанного ключа
        System.out.println("headMap все значения до 3: " + headMap);

        SortedMap<Integer, String> tailMap = sortedMap.tailMap(3);  // берёт все значения из другой мап начиная с указанного ключа
        System.out.println("tailMap все значения начиная с 3: " + tailMap);

        System.out.println();
        System.out.println("------------------------------------------------");
        System.out.println("Тема NavigableMap");

        NavigableMap<Integer, String> navigableMap = new TreeMap<>(); // NavigableMap работает только с TreeMap
        for (int i = 0; i <= 5; i++) {
            navigableMap.put(i, "Value: " + i);
        }

        System.out.println("navigableMap: " + navigableMap);

        Integer lowerKey = navigableMap.lowerKey(3); // Меньше 3
        Map.Entry<Integer, String> lowerEntry = navigableMap.lowerEntry(3); // Меньше 3

        Integer floorKey = navigableMap.floorKey(3); // Меньше или = 3
        Map.Entry<Integer, String> floorEntry = navigableMap.floorEntry(3); // Меньше или = 3

        Integer higherKey = navigableMap.higherKey(3); // Больше 3
        Map.Entry<Integer, String> higherEntry = navigableMap.higherEntry(3); // Больше 3

        Integer ceilingKey = navigableMap.ceilingKey(3); // Больше или = 3
        Map.Entry<Integer, String> ceilingEntry = navigableMap.ceilingEntry(3); // Больше или = 3

        System.out.println("lowerKey: " + lowerKey);
        System.out.println("lowerEntry: " + lowerEntry);

        System.out.println("floorKey: " + floorKey);
        System.out.println("floorEntry: " + floorEntry);

        System.out.println("higherKey: " + higherKey);
        System.out.println("higherEntry: " + higherEntry);

        System.out.println("ceilingKey: " + ceilingKey);
        System.out.println("ceilingEntry: " + ceilingEntry);

        NavigableMap<Integer, String> descendingMap = navigableMap.descendingMap();
        System.out.println("descendingMap: " + descendingMap); // В обратном порядке
    }
}