package MyCode.QueueTest;

import java.util.ArrayDeque;
import java.util.Queue;

public class QueueTest {
    public static void main(String[] args) {
        Queue<Integer> queue = new ArrayDeque<>();
        queue.offer(10);
        queue.offer(20);

        //20, 10 (head)

        System.out.println("headElem: " + queue.element()); //Element и peek одно и тоже. Разница, что peek не выбрасывает исключение если нет элемента
        System.out.println("headElem: " + queue.peek()); // методы показывают элемент в head

        queue.offer(30);

        //30, 20, 10 (head)

        Integer value = queue.remove(); //сохраняет значение из head и удаляет его из очереди (можно использовать метод poll - то же самое, что remove, но без исключения)
        System.out.println("value: " + value);

        // (tail) 30, 20 (head)

        System.out.println("headElem: " + queue.peek());

        while (!queue.isEmpty()) {
            System.out.println(queue.poll());
        }
    }
}