package MyCode.AnonimClassAndLambda;

import java.util.ArrayList;
import java.util.List;

public class AnonimAndLambdaTest {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("a");
        strings.add("abbb");
        strings.add("avvvv");

        //Создаётся анонимный класс. Переопределяется метод интерфейса MyPredicate
        MyPredicate<String> predicate = new MyPredicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() > 2;
            }
        };

        //пробегает по листу и выводит строки соответствующие условию в предикате
        for (String s : strings) {
            if (predicate.test(s))
                System.out.println(s);
        }


        // То же самое, что и выше только с лямбдой
        List<String> strings2 = new ArrayList<>();
        strings.add("a");
        strings.add("abbb");
        strings.add("avvvv");

        //лямба
        MyPredicate<String> predicate2 = s -> s.length() > 2;

        for (String s : strings) {
            if(predicate.test(s))
                System.out.println(s);
        }
    }
}