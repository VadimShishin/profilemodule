package MyCode.AnonimClassAndLambda;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class AnonimAndLambdaTest2 {
    public static void main(String[] args) {

        // Проверка с помощью анонимного класса
        System.out.println("Predicate");
        Predicate<Integer> test1 = new Predicate<Integer>() {
            @Override
            public boolean test(Integer digit) {
                if (digit >= 7) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        System.out.println("test1. 10 >= 7: " + test1.test(10));
        System.out.println("test1. 5 >= 7: " + test1.test(5));
        System.out.println();

        //----------------------------------------------------------------

        // Такая же проверка с помощью лямбды (не трансформируется в анонимный класс)
        System.out.println("Predicate");

        Predicate<Integer> test2 = (Integer digit) -> {
            if (digit >= 7) {
                return true;
            } else {
                return false;
            }
        };

        System.out.println("test2. 10 >= 7: " + test2.test(10));
        System.out.println("test2. 5 >= 7: " + test2.test(5));
        System.out.println();

        //----------------------------------------------------------------

        System.out.println("BiPredicate");

        BiPredicate<Integer, Integer> test3 = (Integer leftDigit, Integer rightDigit) -> {
            if (leftDigit + rightDigit >= 7) {
                return true;
            } else {
                return false;
            }
        };

        System.out.println("test3. 10 + 2 >= 7: " + test3.test(10, 2));
        System.out.println("test3. 5 + 1 >= 7: " + test3.test(5, 1));
        System.out.println();

        //----------------------------------------------------------------

        System.out.println("Supplier");

        Supplier<Integer> test4 = () -> {
            return 666;
        };

        System.out.println("test4. get 666" + test4.get());
        System.out.println();

//        Supplier<Exception> test5 = () -> {
//            return new Exception("Some problem happened");
//        };
//
//        System.out.println("Supplier + Exception");
//        Exception ex = test5.get();
//        throw new RuntimeException(ex);

        //----------------------------------------------------------------

        System.out.println("Consumer");

        Consumer<String> test6 = (String str) -> {
            System.out.println(str);
        };

        test6.accept("test6. accept");
        System.out.println();

        //----------------------------------------------------------------

        System.out.println("Function");

        Function<Integer, String> test7 = (Integer digit) -> {
            return String.valueOf(digit);
        };

        System.out.println("test7. Convert int to string: " + test7.apply(666));
        System.out.println();

        //----------------------------------------------------------------

        System.out.println("Function");

        Function<Integer, Integer> test8 = (Integer digit) -> {
            return digit * 2;
        };

        System.out.println("test8. 666 * 2: " + test8.apply(666));
        System.out.println();

        //----------------------------------------------------------------

        // UnaryOperator наследуется от Function

        System.out.println("UnaryOperator");

        UnaryOperator<Integer> test9 = (Integer digit) -> {
            return digit * 2;
        };

        System.out.println("test9. 666 * 2: " + test9.apply(666));
        System.out.println();

        //----------------------------------------------------------------

        System.out.println("UnaryOperator в примере с листом");

        List<String> list = new ArrayList<>();
        list.add("AA");
        list.add("BB");
        list.add("CC");

        UnaryOperator<String> test9b = (String s) -> {
            return s + " + " + s;
        };

        list.replaceAll(test9b);

        //вывод через лямбду и consumer
        list.forEach((String s) -> {
            System.out.println(s);
        });
        System.out.println();

        //----------------------------------------------------------------

        System.out.println("IntConsumer");

        IntConsumer test10 = (int i) -> {
            System.out.println(i * 2);
        };

        test10.accept(666);
        System.out.println();

        //----------------------------------------------------------------

        // сортировка с помощью компаратора
        System.out.println("Comparator. Сортировка");

        List<String> list2 = new ArrayList<>();
        list2.add("2");
        list2.add("1");
        list2.add("3");

        Comparator<String> test11 = (String lhs, String rhs) -> {
            return lhs.compareTo(rhs);
        };

        list2.sort(test11);

        list2.forEach((String s) -> {
            System.out.print(s);
            System.out.println(" ");
        });
        System.out.println();

        //----------------------------------------------------------------

        // сортировка по длине строки с помощью компаратора
        System.out.println("Comparator. Сортировка по длине строки");

        List<String> list3 = new ArrayList<>();
        list3.add("AAA");
        list3.add("AA");
        list3.add("A");

        Comparator<String> test12 = (String lhs, String rhs) -> {
            int lhsSize = lhs.length();
            int rhsSize = rhs.length();

            if (lhsSize == rhsSize) {
                return 0;
            }

            if (lhsSize < rhsSize) {
                return -1;
            } else {
                return 1;
            }
        };

        list3.sort(test12);

        list3.forEach((String s) -> {
            System.out.print(s);
            System.out.println(" ");
        });
    }
}
