package DZ02.Task04;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPageCount() {
        return pageCount;
    }

    @Override
    public String toString() {
        return "Document(" +
                "id = " + id +
                ", name = " + name +
                ", pageCount = " + pageCount + ")";
    }
}

