package DZ02.Task04;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// В некоторой организации хранятся документы (см. класс Document). Сейчас все документы лежат в ArrayList, из-за чего
// поиск по id документа выполняется неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам перевести
// хранение документов из ArrayList в HashMap.

public class Main {
    public static void main(String[] args) {
        ArrayList<Document> list = new ArrayList<>();

        list.add(new Document(1, "1 document", 100));
        list.add(new Document(2, "2 document", 200));
        list.add(new Document(3, "3 document", 300));

        Map<Integer, Document> map = organizeDocuments(list);

        System.out.println(map);
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();

        for (Document document : documents) {
            map.put(document.getId(), document);
        }

        return map;
    }
}