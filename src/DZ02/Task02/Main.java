package DZ02.Task02;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        String t = input.nextLine();

        ArrayList<String> list1 = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            list1.add(String.valueOf(s.toLowerCase().charAt(i)));
        }

        ArrayList<String> list2 = new ArrayList<>();
        for (int i = 0; i < t.length(); i++) {
            list2.add(String.valueOf(t.toLowerCase().charAt(i)));
        }

        System.out.println(list1.containsAll(list2));
    }
}