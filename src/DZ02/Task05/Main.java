package DZ02.Task05;


import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

//Реализовать метод, который принимает массив words и целое положительное число k. Необходимо вернуть k наиболее часто встречающихся слов.
//Результирующий массив должен быть отсортирован по убыванию частоты встречаемого слова. В случае одинакового количества
// частоты для слов, то отсортировать и выводить их по убыванию в лексикографическом порядке.

public class Main {
    public static void main(String[] args) {
//        String[] array = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
//        String[] array2 = {"a", "b", "b", "c", "a", "d", "d", "c", "b", "a", "a"};
//        int k = 4;
//        System.out.println(Arrays.toString(mostCommonWords(array, k)));

    }

    public static String[] mostCommonWords(String[] words, int k) {
        Map<String, Integer> map = new TreeMap<>();

        for (String tmp : words) {
            map.put(tmp, map.getOrDefault(tmp, 0) + 1);
        }

        PriorityQueue<Map.Entry<String, Integer>> maxHeap = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
        maxHeap.addAll(map.entrySet());

        ArrayList<Map.Entry<String, Integer>> sorted = new ArrayList(Collections.singleton(maxHeap.addAll(map.entrySet())));


        String[] topKWords = new String[k];
//            topKWords[i] = maxHeap.poll().getKey();
        return topKWords;

        //        var map = new HashMap<String, Integer>();
        //        var words = List.of("the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day");
        //        words.forEach(word ->
        //                map.merge(word, 1, (prev, one) -> prev + one)
        //        );
        //        System.out.println(map);


    }
}
