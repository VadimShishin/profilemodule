package DZ02.Task01;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(3);
        list.add(4);

        System.out.println(method(list));
    }

    public static TreeSet<Integer> method(ArrayList<Integer> list) {
        TreeSet<Integer> anotherList = new TreeSet<>(list);
        return anotherList;
    }
}
