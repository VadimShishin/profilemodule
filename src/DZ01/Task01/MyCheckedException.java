package DZ01.Task01;

public class MyCheckedException extends Exception {
    public MyCheckedException(String message){
        super(message);
    }
}
