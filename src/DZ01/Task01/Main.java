package DZ01.Task01;

public class Main {
    public static void main(String[] args) {
        int result = division(10, 0);
        System.out.println(result);
    }
    public static int division(int a, int b) {
        if (b == 0) {
            try {
                throw new MyCheckedException("The divisor cannot be zero");
            } catch (MyCheckedException e) {
                System.err.println(e.getMessage());
                return 0;
            }
        }
        return a / b;
    }
}