package DZ01.Task02;

import DZ01.Task01.MyCheckedException;

public class Main {
    public static void main(String[] args) {
        int result = division(10, 0);
        System.out.println(result);
    }
    public static int division(int a, int b) {
        if (b == 0) {
            try {
                throw new MyUncheckedException("The divisor cannot be zero");
            } catch (MyUncheckedException e) {
                System.err.println(e.getMessage());
                return 0;
            }
        }
        return a / b;
    }
}