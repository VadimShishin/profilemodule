package DZ01.Task06;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {
    private String name;
    private Date date;
    private Gender gender;
    private double height;

    //длина имени должна быть от 2 до 20 символов, первая буква заглавная
    public void checkName(String str) {
        if (str.length() > 2 && str.length() < 20 && str.charAt(0) == str.toUpperCase().charAt(0)) {
            this.name = str;
        } else {
            throw new IllegalArgumentException("Длина имени должна быть от 2 до 20 символов");
        }
    }

    //дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты
    public void checkBirthdate(String str) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");
        String inputDataStr = str;
        String bottomLineDateStr = "01.01.1900";
        Date currentNow = new Date();
        Date inputData = format.parse(inputDataStr);
        Date bottomLineDate = format.parse(bottomLineDateStr);

        if (inputData.before(currentNow) || inputData.after(bottomLineDate)) {
            this.date = inputData;
        } else {
            throw new IllegalArgumentException("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
        }
    }

    //пол должен корректно матчится в enum Gender, хранящий Male и Female значения
    public void checkGender(String str) {
        Gender gender = Gender.valueOf(str);
        if (gender == Gender.Male || gender == Gender.Female) {
            this.gender = gender;
        } else {
            throw new IllegalArgumentException("Gender must be");
        }
    }

    //рост должен быть положительным числом и корректно конвертироваться в double
    public void checkHeight(String str) {
        String rostStr = "190";
        double rostDouble = Double.parseDouble(rostStr);
        System.out.println(rostDouble);
    }
}