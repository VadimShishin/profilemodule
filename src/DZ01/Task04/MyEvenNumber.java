package DZ01.Task04;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws InvalidNumberException{
        if (n % 2 == 0) {
            this.n = n;
        } else {
            throw new InvalidNumberException(n);
        }
    }
}
