package DZ01.Task04;

public class InvalidNumberException extends Exception {
    private int n;

    public InvalidNumberException(int n) {
        super("Число должно быть чётным. Вы ввели: " + n);
        this.n = n;
    }

    public int getN() {
        return n;
    }
}
