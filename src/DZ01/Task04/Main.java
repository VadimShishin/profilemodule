package DZ01.Task04;

public class Main {
    public static void main(String[] args) {
        try {
            MyEvenNumber number1 = new MyEvenNumber(4);
            MyEvenNumber number2 = new MyEvenNumber(3);
        } catch (InvalidNumberException e){
            System.err.println(e.getMessage());
        }
    }
}
