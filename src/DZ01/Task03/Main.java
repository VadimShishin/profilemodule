package DZ01.Task03;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String s = "";
        StringBuilder stringBuilder = new StringBuilder();
        char c;

        try (Scanner scanner = new Scanner(new File("C:/Users/Star/IdeaProjects/ProfileModule/src/DZ01/Task03/input.txt"));
             PrintWriter writer = new PrintWriter("C:/Users/Star/IdeaProjects/ProfileModule/src/DZ01/Task03/output.txt")) {
            while (scanner.hasNextLine()) {
                s = scanner.nextLine();
                for (int i = 0; i < s.length(); i++){
                    c = s.charAt(i);
                    stringBuilder.append(proverka(c));
                }
                writer.println(stringBuilder);
                stringBuilder.setLength(0);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
    public static char proverka (char c) {
        if (c == 'a' || c == 'b' || c == 'c' || c == 'd' || c == 'e' || c == 'f' || c == 'g' || c == 'h' || c == 'i' || c == 'j'|| c == 'k'|| c == 'l' || c == 'm' || c == 'n' || c == 'o' || c == 'p'|| c == 'q' || c == 'r' || c == 's' || c == 't' || c == 'u' || c == 'v' || c == 'w' || c == 'x' || c == 'y' || c == 'z') {
            return Character.toUpperCase(c);
        } else {
            return c;
        }
    }
}