package Pracktikum.Week4.Nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/*
Копировать файл с контентом
 */
public class Task3 {
    public static void main(String[] args) {
        Path sourcePath = Paths.get("src/Pracktikum/Week4/Nio/newPackageFolder/childSubDirectory/test.txt");
        Path targetPath = Paths.get("src/Pracktikum/Week4/Nio/newPackageFolder/childSubDirectory/test_copy.txt");

        try {
            Path path = Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Target file path: " + path);
            System.out.println("Copied content: \n " + new String(Files.readAllBytes(path)));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }
}