package Pracktikum.Week4.Functional.Task2;

@FunctionalInterface
public interface Square {
    public int calculateSquare(int i);
}
