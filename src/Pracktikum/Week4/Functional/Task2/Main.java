package Pracktikum.Week4.Functional.Task2;
/*
С помощью функционального интерфейса выполнить подсчет квадрата числа
 */

public class Main {
    public static void main(String[] args) {

        // Анонимный класс
        Square square = new Square() {

            @Override
            public int calculateSquare(int i) {
                return i * i;
            }
        };

        System.out.println(square.calculateSquare(5));

        // Лямбда
        Square squareLambda = i -> i * i;
        System.out.println(squareLambda.calculateSquare(5));

    }
}
