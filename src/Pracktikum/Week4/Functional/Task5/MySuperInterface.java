package Pracktikum.Week4.Functional.Task5;
@FunctionalInterface
public interface MySuperInterface<T> {
    T func(T value);
}
