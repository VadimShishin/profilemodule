package Pracktikum.Week4.Functional.Task3;
/*
получить значение PI через собственный функциональный интерфейс
 */

public class Main {
    public static void main(String[] args) {
        MyInterface myInterface1;
        myInterface1 = () -> Math.PI;
        System.out.println(myInterface1.getPiValue());

        MyInterface myInterface2 = () -> 3.14;
        System.out.println(myInterface2.getPiValue());
    }
}
