package Pracktikum.Week4.Functional;

public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Создал функциональный интерфейс!");
            }
        }).start();

        new Thread(() -> System.out.println("Я тоже создан функциональный интерфейс!")).start();

        Runnable runnable = () -> System.out.println("Hello world");
        runnable.run();

    }
}
