package Pracktikum.Week4.Functional.Task1;

public class Timer {
    public long timeNanoSeconds = 0;
    double elapsedTimeInSecond = 0;

    public void measureTime(Runnable runnable) {
        long startTime = System.nanoTime();
        runnable.run();
        timeNanoSeconds = System.nanoTime() - startTime;
        elapsedTimeInSecond = (double) timeNanoSeconds / 1_000_000_000;
    }
}