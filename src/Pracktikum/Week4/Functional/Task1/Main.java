package Pracktikum.Week4.Functional.Task1;
/*
Создать таймер, который считает время выполнения метода, используя Runnable.
 */

public class Main {
    public static void main(String[] args) {

        //До java 7
        System.out.println("До java 7:");
        Timer timer = new Timer();
        timer.measureTime(new SimpleSummator());
        System.out.println(timer.timeNanoSeconds); // в наносекундах
        System.out.println(timer.elapsedTimeInSecond); // в секундах

        //Java 7. Анонимный класс
        System.out.println();
        System.out.println("Java 7. Анонимный класс:");
        Timer timerAnonymous = new Timer();
        timerAnonymous.measureTime(new Runnable() {
            @Override
            public void run() {
                long sum = 0;
                for (int i = 1; i <= 1_000_000_000; i++) {
                    sum += 1;
                }

                System.out.println("Результат: " + sum);
            }
        });

        System.out.println(timerAnonymous.timeNanoSeconds);
        System.out.println(timerAnonymous.elapsedTimeInSecond);

        //Java 8. Лямбда выражения - Lambda
        System.out.println();
        System.out.println("Java 8. Лямбда выражения - Lambda:");
        Timer lambdaTimer = new Timer();
        lambdaTimer.measureTime(() -> {
            long sum = 0;
            for (int i = 1; i <= 1_000_000_000; i++) {
                sum += 1;
            }

            System.out.println("Результат: " + sum);
        });

        System.out.println(lambdaTimer.timeNanoSeconds);
        System.out.println(lambdaTimer.elapsedTimeInSecond);
    }
}