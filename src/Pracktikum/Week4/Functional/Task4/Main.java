package Pracktikum.Week4.Functional.Task4;
/*
реализовать метод, чтобы вывести строку наоборот, используя наш ReverseInterface
 */

public class Main {
    public static void main(String[] args) {
        ReversedInterface reversedInterface = (String str) -> new StringBuilder(str)
                .reverse()
                .toString();

        System.out.println(reversedInterface.getReversedString("Reversed string"));

        ReversedInterface reversedInterface2 = (str) -> {
            String result = "";
            for (int i = str.length() - 1; i >= 0; i--) {
                result += str.charAt(i);
            }
            return result;
        };

        System.out.println(reversedInterface2.getReversedString("gnirts desreveR"));
    }
}
