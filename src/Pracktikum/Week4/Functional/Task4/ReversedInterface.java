package Pracktikum.Week4.Functional.Task4;
@FunctionalInterface
public interface ReversedInterface {
    String getReversedString(String initialString);
}
