package Pracktikum.Week4.Streams.Task1;
import Pracktikum.Week4.Functional.Task2.Square;

import java.util.List;

/*
Использовать реализованный функциональный интерфейс Square (Task2) на списке чисел, вывести на экран.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        Square square = x -> x * x;

        list.stream()
                .map(square::calculateSquare)
                .forEach(System.out::println);

    }
}