package Pracktikum.Week3.Annotation.Task6;
import Pracktikum.Week3.Annotation.Task5.*;

public class ClassDescriptionSaver {
    public static void writeDescription(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(ClassDescription.class)) {
            return;
        }

        ClassDescription classDescription = clazz.getAnnotation(ClassDescription.class);

        System.out.println("Автор: " + classDescription.author());
        System.out.println("Дата создания: " + classDescription.date());
        System.out.println("Текущая версия: " + classDescription.currentRevision());

        System.out.println("Список проверяющих: ");
        for (String reviewer : classDescription.reviewers()) {
            System.out.println("> " + reviewer);
        }
    }

    public static void main(String[] args) {
        writeDescription(PerfectClass.class);
    }
}