package Pracktikum.Week3.Annotation.examples;

public interface Summable {
    int sum(int a, int b);
}