package Pracktikum.Week3.Annotation.Task7;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class LoggingService {
    public static void log(Object obj, String methodName) {

        Class<?> clazz = obj.getClass();
        for (Method m : clazz.getMethods()) {
            if (m.getName().equals(methodName)) {
                for (Annotation a : m.getAnnotations()) {
                    if (a instanceof ToBeLogged lmc) {
                        switch (lmc.logLevel()) {
                            case INFO ->
                                    System.out.println("[INFO] Logging method: " + m.getName());
                            case DEBUG ->
                                    System.out.println("[DEBUG] Logging method: " + m.getName() + " at time: " + LocalDateTime.now());
                        }

                    }
                }
                break;
            }
        }
    }
}