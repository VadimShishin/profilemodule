package Pracktikum.Week3.Reflection.materials;

public class InterfaceExample {

    interface A {
    }

    interface B {
    }

    interface Z {}

    static class C implements A {
    }

    static class D extends C implements B, Z {
    }

    public static void main(String[] args) {
        for (Class<?> cls : D.class.getInterfaces()) {
            System.out.println(cls.getName());
        }

        System.out.println(D.class.getSuperclass());
    }
}
