package Pracktikum.Week3.Reflection.Task1;

import Pracktikum.Week3.Reflection.Task1.struckture.D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Получить все интерфейсы класса, включая интерфейсы от классов-родителей.
Не включать интерфейсы родительских интерфейсов.
 */

public class Task1 {
    //код преподавателя
    public static List<Class<?>> getAllInterfaces(Class<?> clazz) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (clazz != Object.class) {
            interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
            clazz = clazz.getSuperclass();
        }
        return interfaces;
    }

    public static void main(String[] args) {
        List<Class<?>> interfaces = getAllInterfaces(D.class);
        for (Class<?> anInterface : interfaces) {
            System.out.println(anInterface.getName());
        }
    }


    //мой код
//    public static void getAllInterfaces(Class<?> type) {
//        System.out.println(Arrays.toString(type.getInterfaces()));
//        System.out.println(Arrays.toString(type.getSuperclass().getInterfaces()));
//    }
//    public static void main(String[] args) {
//        getAllInterfaces(D.class);
//    }
}