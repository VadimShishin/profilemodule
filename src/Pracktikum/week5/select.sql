create table public.books
(
    id          serial primary key,
    title       varchar(100) not null,
    author      varchar(150) not null,
    data_added  timestamp not null
);

INSERT INTO books(title, author, data_added)
VALUES ('Недоросль', 'Д. И. Фонвизин', now());
INSERT INTO books(title, author, data_added)
VALUES ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - INTERVAL '24h');
INSERT INTO books(title, author, data_added)
VALUES ('Доктор Живаго', 'Б. Л. Пастернак', now() - INTERVAL '24h');
INSERT INTO books(title, author, data_added)
VALUES ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());

SELECT * FROM books;

--1. Достать запись под id = 4

SELECT * --показать cтроку по требуемому id
FROM books
WHERE id = 4;

SELECT title, author --отобразить необходимые столбцы
FROM books
WHERE id = 4;

SELECT title AS ONE, author AS two --отобразить необходимые столбцы, переименовав столбцы
FROM books
WHERE id = 4;

SELECT title AS "Название", author AS "Автор" --Если на русском, то необходимо писать в ковычках
FROM books
WHERE id = 4;

--2. Найти автора книги по названию ‘Недоросль’
SELECT *
FROM books
WHERE title = 'Недоросль';

--3. Найти все книги Пастернака
SELECT *
FROM books
WHERE trim(author) ILIKE '%пастернак';

--4. Вывести максимальный id в таблице
SELECT max(id)
FROM books;

--общее количество записей(строк) в таблице
SELECT count(*) as count_books
FROM books;
--подобные функции:
--avg, min, sum, count, mod, power....

SELECT * FROM books;
--5. Найти все книги Радищева или Пастернака отсортированные по дате в обратном порядке
SELECT *
FROM books
WHERE trim(author) ILIKE '%пастернак'
OR trim(author) ILIKE '%радищев'
ORDER BY data_added DESC; --ASK по умолчанию - по порядку, DESK - в обратном порядке

SELECT *
FROM books
WHERE trim(author) ILIKE '%пастернак'
--union
UNION ALL --склеивает
SELECT *
FROM books
WHERE trim(author) ILIKE '%радищев'
ORDER BY data_added DESC;
--union берёт только уникальные. union all возьмет все. Примеры:
--union = 1 2 3 4 5 6 7
--union all = 1 2 2 3 3 4 4 5 6 7

--6. Найти все книги Пастернака добавленные вчера
SELECT count()
FROM books
WHERE trim(author) ILIKE '%пастернак'
AND data_added <= now() - interval '24h';