/*
Создать таблицу книг со следующими полями
- id ключ
- title- наименование книги
- author - автор книги
- date_added - дата добавления книги
 */
--DDL - Data Definition Language (создание таблиц/сиквенсов и тп)

create table public.books
(
    id          serial primary key, --первичный ключ
    title       varchar(30) not null,
    author      varchar(30) not null,
    data_added  timestamp not null
);

--sequence - последовательность, например от 1 до 9999999999999999999, шаг = 1 (автоинкремент)
--insert int books (...) values (sequence.next(),...)

select *
from books;

--DML - Data Manipulation Language (манипуляции с данными, создание/обновление/удаление ДАННЫХ)!
--CRUD - Create/Read/Update/Delete

insert into books(title, author, data_added)
values ('Недоросль', 'Д. И. Фонвизин', now());

--Если не указываем поля в insert, нужно вставлять все данные

INSERT INTO books
VALUES (nextval('books_id_seq'), 'Недоросль', 'Д. И. Фонвизин', now());

insert into books(title, author, data_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');

insert into books(title, author, data_added)
values ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');

insert into books(title, author, data_added)
values ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());


--Изменение полей/типов данных в таблице

alter table books
    alter column title type varchar(100);

alter table books
    alter column author type varchar(100);

alter table books
    alter column author type varchar(100) using author::varchar(100);

---------удаление---------
--удаление конкретной строки

DELETE
FROM books
WHERE id = 2;

DELETE
FROM books
WHERE id IN (SELECT id FROM books LIMIT 2); -- УДАЛЯЕТ 1 И 3 СТРОКУ (2 УЖЕ УДАЛЕНА РАНЕЕ)

SELECT * FROM books;

--удаление всех строк
TRUNCATE TABLE books;

-- delete from books -- можно писать условия


--удалить таблицу целиком
DROP TABLE books;