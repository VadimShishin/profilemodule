package DZ03.Task02;

import DZ03.Task01.*;

/*
	Написать метод, который рефлексивно проверит наличие аннотации @IsLike на любом переданном классе и выведет значение, хранящееся в аннотации, на экран.
 */

public class Task02 {
    public static void main(String[] args) {
        checkingAnnotation(Task01.class);
    }

    public static void checkingAnnotation(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(IsLike.class)) {
            return;
        }

        IsLike isLike = clazz.getAnnotation(IsLike.class);

        System.out.println(isLike.like());
    }
}
