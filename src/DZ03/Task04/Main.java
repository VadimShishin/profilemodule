package DZ03.Task04;

/*
Написать метод, который с помощью рефлексии получит все интерфейсы класса, включая интерфейсы от классов-родителей и интерфейсов-родителей
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Class<?>> interfaces = getAllInterfaces(String.class);
        for (Class<?> anInterface : interfaces) {
            System.out.println(anInterface.getName());
        }
    }

    public static Set<Class<?>> getAllInterfaces(Class<?> clazz) {
        Set<Class<?>> interfaces = new HashSet<>();

        while (clazz != Object.class) {
            interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
            clazz = clazz.getSuperclass();
        }

        for (Class<?> interFace : interfaces) {
            while (interFace != null) {
                interfaces.addAll(Arrays.asList(interFace.getInterfaces()));
                interFace = interFace.getSuperclass();
            }
        }

        return interfaces;
    }
}