package DZ03.Task01;

/*
	Создать аннотацию @IsLike, применимую к классу во время выполнения программы. Аннотация может хранить boolean значение.
 */

@IsLike(
        like = true
)
public class Task01 {
}
