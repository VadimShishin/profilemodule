package DZ03.Task03;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
    С помощью рефлексии вызвать метод print() и обработать все возможные ошибки (в качестве аргумента передавать любое
    подходящее число). При “ловле” исключений выводить на экран краткое описание ошибки
 */
public class Main {
    public static void main(String[] args) {
        APrinter printer = new APrinter();
        Class<? extends APrinter> cl = printer.getClass();

        try {
            Method method = cl.getMethod("print", int.class);
            method.invoke(printer, 666);
        } catch (NoSuchMethodException e) {
            System.out.println("Нет такого метода");
        } catch (InvocationTargetException e) {
            System.out.println("InvocationTargetException");
        } catch (IllegalAccessException e) {
            System.out.println("Нет доступа");
        }
    }
}