package DZ04.Task5;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 На вход подается список непустых строк. Необходимо привести все символы строк к верхнему регистру и вывести их, разделяя запятой.
 Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(Stream.of("abc", "def", "qqq").map(String::toUpperCase).collect(Collectors.joining(", ")));
    }
}
