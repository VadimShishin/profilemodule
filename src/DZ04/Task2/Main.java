package DZ04.Task2;

import java.util.Scanner;
import java.util.stream.Stream;

//На вход подается список целых чисел. Необходимо вывести результат перемножения этих чисел.
//Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число 120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
public class Main {
    public static void main(String[] args) {
        // 1 вариант. Упрощённый
        System.out.println(Stream.of(1, 2, 3, 4, 5).reduce((x, y) -> x * y).get());

        // 2 вариант. Со значениями введёнными из консоли
//        Scanner input = new Scanner(System.in);
//
//        Stream.Builder<Integer> streamBuider = Stream.builder();
//        for (int i = 1; i <= 5; i++) {
//            streamBuider.accept(Integer.valueOf(input.next()));
//        }
//
//        streamBuider.build().reduce((x, y) -> x * y).ifPresent(System.out::println);
    }
}
