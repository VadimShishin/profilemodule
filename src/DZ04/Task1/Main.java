package DZ04.Task1;

import java.util.stream.IntStream;

// Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на экран
public class Main {
    public static void main(String[] args) {
        System.out.println(IntStream.rangeClosed(1, 100).filter(x -> x % 2 == 0).sum());
    }
}