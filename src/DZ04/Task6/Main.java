package DZ04.Task6;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

//Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSets = Set.of(Set.of(1, 2, 3), Set.of(4, 5, 6), Set.of(7, 8, 9, 10));
        Set<Object> flatSet = setOfSets.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        System.out.println(flatSet);
    }
}
