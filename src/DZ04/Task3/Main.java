package DZ04.Task3;

import java.util.stream.Stream;

/**
 На вход подается список строк. Необходимо вывести количество непустых строк в списке.
 Например, для List.of("abc", "", "", "def", "qqq") результат равен 3
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(Stream.of("abc", "", "", "def", "qqq").filter(s -> !s.isEmpty()).count());
    }
}
