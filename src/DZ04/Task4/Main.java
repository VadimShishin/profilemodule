package DZ04.Task4;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

//На вход подается список вещественных чисел. Необходимо отсортировать их по убыванию
public class Main {
    public static void main(String[] args) {
        Stream.of(5, 1, 4, 2, 3)
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);

        //Вариант с Math::random
//        List<Double> stream = Stream
//                .generate(Math::random)
//                .limit(5)
//                .sorted(Comparator.reverseOrder())
//                .toList();
//        stream.forEach(System.out::println);
    }
}
